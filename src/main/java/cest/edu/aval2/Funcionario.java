package cest.edu.aval2;

import java.math.BigDecimal;


public class Funcionario extends cest.edu.aval2.PessoaFisica {

	private String cargo;
	private BigDecimal salario;
	
	public Funcionario(String nome, String cpf, String cargo, BigDecimal salario) {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String getNome() {
		// TODO Auto-generated method stub
		return super.getNome();
	}
	@Override
	public String getCPF() {
		// TODO Auto-generated method stub
		return super.getCPF();
	}
	public String getCargo() {
		return cargo;
	}

	public BigDecimal getSalario() {
		return salario;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public void setSalario(BigDecimal salario) {
		this.salario = salario;
	}
	
}
