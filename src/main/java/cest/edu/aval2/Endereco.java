package cest.edu.aval2;

public class Endereco {
	private String logradouro;
	private Cidade cidade;
	private int numero;
	
	public Endereco(String logradouro, Cidade cidade, int numero) {		
		this.logradouro = logradouro;
		this.cidade = cidade;
		this.numero = numero;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public Cidade getCidade() {
		return cidade;
	}
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
		
}
