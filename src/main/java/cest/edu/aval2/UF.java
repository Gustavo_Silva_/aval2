package cest.edu.aval2;

public class UF {
	private String sigla;
	private String descricao;
	
	public UF(String descricao, String sigla) {
		this.descricao = descricao;
		this.sigla = sigla;
	}
	public String getDescricao() {
		return descricao;
	}
	public String getSigla() {
		return sigla;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
}

