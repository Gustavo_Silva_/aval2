package cest.edu.aval2;


public class Contador {
	    private int numero = 0;
	    
	    private static Contador g = new Contador(); 
		//Eu escolhe "g" porque é a inicial do meu nome.
		private Contador() {
		}	
		public static Contador getInstance() { 
			return g;
		}
		public int proximoNumero() { 
			int i = 0;
			return ++ i;
		}
		public int getNumero() {
			return numero;
		}
		
		public void setNumero(int numero) {
			this.numero = numero;
		}
	}
