package cest.edu.aval2;

import java.util.Date;

public class Atendimento {
	private String nome;
	private Date data;
	private int numero;
	
	public Atendimento(String nome, Date data, int numero) {		
		this.nome = nome;
		this.data = data;
		this.numero = numero;
	}
	public String getNome() {
		return nome;
	}
	public Date getData() {
		return data;
	}

	public int getNumero() {
		return numero;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setData(Date data) {
		this.data = data;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}	
	
}
